// color truth tables
// green for T
// red for F
elements = document.getElementsByTagName('td');
for (var i in elements) {
  if (!isNaN(Number(i))) {
    element = elements[i];
    if (element.innerText == 'T') element.style.backgroundColor = "#33cc33";
    if (element.innerText == 'F') element.style.backgroundColor = "#ff3333";
    if (element.innerText == 'T' || element.innerText == 'F') element.style.textAlign = "center";
  }
}
